# How to Make Kites

You will need:

 * 2m white cotton
 * 5.5m Ribbon
 * Tracing Paper
 * Graph paper
 * Clear Acrylic sheet- 6mm by 450mm × 800
 * A 3.5×2.5m 12mm thick birch plywood sheet cut into 6, 2.5m long, 63mm wide strips, plus the remaining flat board,
 * Anemometer, PCB + components
 * 5V, 4A DC power supply
 * coil winding jig
 * 45 WS2812b addressable RGB LEDs on a flexible strip,
 * Metal cabling (support cable) 2mm 7x7 A4-AISI 316 stainless steel cable 5m
 * 4× PVC end caps for 2mm cable
 * 4× Rope Thimbles for 2mm cable
 * 4× stainless steel wire rope clips for 2mm
 * 1× stainless steel hook and eye turnbuckle
 * M6×40mm stainless steel eye bolt
 * v-groove pulleys with 605 bearings
 * M6 threaded rod
 * M6×30 button cap bolts
 * M6 nylock nuts
 * M6 full nuts
 * M6 penny washers
 * 1× reel of 42 AWG enamelled Copper wire

All the design files, Arduino code and this build guide are all linked from:
[defproc.co.uk/p/kites](https://git.defproc.co.uk/red-violet-made/kites/tree/master)

## Make the fabric Kites

Draw Kite this shape on graph paper — the finished kite is 14cm high and 6cm
wide, with 1cm seam allowance that is trimmed back to the final size.

![kite template](images/kite.png)
***Kite template***

Print out, or trace, then cut out around the green outer edge either. The template will be used 45 times, so more than one may be required, depending on use.

Using a very light weight cotton fabric, fold the fabric in half and pin the Kite template to the folded fabric.

Once you have cut out 45 on the folded fabric you should have 90 individual kites.

Each individual kite now needs to be folded and ironed, first down the length from
point to point and secondly across from point to point, following the red dot-dash lines on the template. When two parts are
placed back to back, this will create the pocket for the LED to sit in.

Cut the Ribbon to length, the long tale is 8cm and the short is 4cm.

You with then need to pin 2 kites together. Makes sure that the creases are opposite from each other so they form a 3D shape. Also you need to insert the Ribbon at the base to form the kites tales.

Stitch together along the blue dashed lines, ensuring that you leave the top points open to allow insertion of the LED. Trim the seam allowance back to the final black outline, 14cm high and 6cm wide.

## Stand

Cut each piece to the drawings.

 * 2× [X-Base](parts/Kites - X-Base.pdf)
 * 2× [Y-Base](parts/Kites - Y-Base.pdf)
 * 2× [Gusset](parts/Kites - Gusset.pdf)
 * 2× [Upright](parts/Kites - Y-Base.pdf)
 * 2× [Swing Arm](parts/Kites - Arm.pdf)

Add the triangular pieces (gusset) to each of the x-base (with 3 holes) to form a symmetric pair. Bolt the outer two holes with M6×30 bolts and nylock nuts, but leave the centre holes empty.

![x-base-gusset](images/x-base-gusset.png)
***X-base and gusset***

Bolt the first upright (with the angle cut end) to the gusset, on the same side as the x-base. The upright should lean back over the longer length of the x-base.

![x-base-gusset-upright](images/x-base-gusset-upright.png)
***X-base, gusset and upright***

Stand the remaining y-base on end, with the cut slots facing upwards. carefully slot each two x-base-&-gusset into them, with the gusset on the inside.

![complete-base](images/complete-base.png)
***Complete base***

To complete the stand base, clamp through the two remaining holes with threaded rod, but slide the eye of the turnbuckle in the centre position.

Attached the swing arm to the upright using square blocks to space them out. Place washers between the rotating arms. Site the swing arm inside the upright and ensure the same amount sticks out from the pivot up from the upright and back from the swing arm.

On the short end of each arm pass threaded rod through the end wholes, clamping a V-groove pulley in the centre of each. Bolt the threaded rod either side of each arm to maintain a consistent distance between them.

At the longer end of the swing arm, run a threaded rod and clamp again, but leave the centre free to mount the tension cable for the whole arm, and the hanging cable for the light wheel.

Mount one end of the tension cable to the plain threaded rod on the long end of the swing arm, using a rope thimble to prevent the cable from making a corner, and a wire rope clip to fasten. Run the cable over the upright V-groove pulley, followed by the swing arm V-groove pulley and bring it down to the hook of the turnbuckle to get an approximate length for the support cable. Add a thimble on the bottom, and a wire rope clip to fix the cable at the approximate location.

![swing-arm](images/swing-arm.png)
***Swing arm***

Trim the wire rope at the top to a short distance from the wire rope grip and cover the cut end with a PVC end cap. Ensure the wire rope grips are used as specified in the safe practice manual. The lower end should be left untrimmed for now, so the hanging assembly can be tested in position, and the cable trimmed to it final length at the end.

## Wheel

![wheel](images/wheel.png)
***The LED support wheel***

Laser cut each part of the wheel assembly from the 6mm clear acrylic sheet. The parts contain 4 holes at each kite/LED position to allow them to hang from the 4 strands of the 42 AWG copper under their own weight.

Parts:

 * 1× [Bottom Plate](parts/Kites - Bottom Plate.pdf)
 * 1× [Top Plate](parts/Kites - Top Plate.pdf)
 * 12× [Hanging Arms](parts/Kites - Hanging Arm.pdf)
 * 12× [Arc Segments](parts/Kites - Arc Segment.pdf)

The whole "wheel" assembly is clamped by only a single bolt, so it should be assembled in the right order. First lay down the lower circular plate (Bottom Plate, with the additional holes) in the centre of a large area.

Secondly stand each of the spokes radially, and aligned vertically, so the each of the tabs on one end fits into each of the slots in the centre hub plate. The 4 holes should be on the lower part when placed.

![add-spokes](images/add-spokes.png)
***Add spokes to the bottom plate***

With all the spokes placed, add each rim section (arc section). They all fit in the same way, and the interlocking curves sit across the slot in each spoke. Adjust the position of the spokes gently to get each one to sit in place.

![add-arc-segment](images/add-arc-segment.png)
***Add each arc segment***

Then add the top hub section, fitting each spoke tab into the respective slot on the hub. At this point, if the two hub plates are kept together, the whole wheel will stay together. To finish, put the stainless steel eye bolt through the two plates and clamp from the bottom.

Assemble a short length of cable that has wire rope clips, rope thimbles and pvc end caps. Pass one end through the hanging eye of the wheel , and connect the other in the centre of the threaded rod at the long end of the swing arm. Now the wheel should hang from the end of the swinging arm.

Adjust the length of the support cable so that the wheel is at a comfortable height to work at.

## Electronics

Assemble the controller board of components. The board is designed to be made single sided, with through hole components, but it could be made by a professional board house. Once complete and all the parts have been soldered, coat the copper surface with clear nail varnish to prevent the copper from oxidising and discolouring. The board is designed to be hot-glue mounted to the centre of the hub, hanging under it, so the anemometer reaches down between the hanging kites.
The Arduino board should be programmed using the arduino sketch. Compiling it will require the FastLED library.

![mounted circuit](images/circuit-in-situ.jpg)
***Mounted controller***

The circuit measures the voltage drop across a diode that is surrounded by a heating coil with constant current. Both the diode and the coil are bolted into screw terminals to make them easier to both place and replace. While the coil must surround the diode completely, it cannot be allowed to touch the leads of the diode or it will connect the two parts of the circuit. coating the diode and the closed area of the leads with clear nail varnish will prevent shorts without affecting the operation of the circuit.

Three connections are made from the circuit board out to the leds, one for 5V power, one for a Ground connection, and one for the led data. All the LEDs in the kites run as a continuous string where the data out from one gets passed on through to the next LED in the chain.

To set up the led wires, 4 connections are made to each LED location, and the 42 AWG wire winds through the wheel structure, additionally binding the structure together, and holding adjacent pieces from rattling.

Wire up the power wires to each LED location. Each location is determined by the position of the 4 holes. be consistent with placement, and run wires all coming from the same position on the hub (where the power connection will go) out to each LED position (The left-most hole from a consistent relative viewing position). Leave the wires long when you cut them off, and leave the inner positions longer than the outer circle.

Then run the ground connections to each LED location. Similar to the 5V power connections, they should all run back to the same point on the hub, this time 180° from the 5V, to where the GND connection will be on the circuit board. Run the wires through the right-most hole and trim to a similar length to the 5V wire.

Then run the data wire first from where the position of the data connection on the board will be, out to the first LED position (using the second from left hole). Then trim the wire to the same length as the others.

Feed a second wire starting from the led position, up through the remaining hole, and out to the next LED position. Although each LED repeats messages out to the next LED in the chain, you should seek to reduce the length of the whole chain, and minimise the overlap.

The actual direction that the led string runs is not critical, as all the animation uses a pseudo-random algorithm. The final LED only needs 3 wires, as the outgoing data line is not used.

Each LED in the flexible strip can be separated by cutting across the strip where the cut line indicates. This leaves each LED in it's own section, with 5V, GND, Data In pads on one side, and 5V, GND, Data Out one the other.

Bend the backing round on each side, so with the LED facing downwards, the pads are all aligned with the vertical plane. Then solder each of the 4 wires hanging down from each LED location, to its respective pad on the LED. As the LED should hang pointing downwards, solder 5V and Data In on one side, and GND and Data Out on the other.

To make it easier to solder, strip the enamel of the end of each wire at the correct length, then tin it with a small amount of solder. With the ED pads similarly tinned, the two can be brought together with the hot end of the soldering iron, which minimises the number of hands required.

Once all the LEDs are placed; strip and solder the initial data connection back to the circuit board, hot-glue the board to the underside of the wheel hub. With the circuit board in place, strip the ends of each power and ground wires, bundle them together, and secure in the respective
screw terminals.

Apply power to the board and test that the LEDs are working correctly. A poor data connection will be indicated by a break in the chain, and bad power or ground connections are indicated by a more green or more red colour, as the WS1812b is being back-powered by the previous LED's data line. Correct all the connections until the LEDs all flicker white.

Run the power cable up the centre of the upright, around the hinge and along the swinging arm. Wind it down the hanging cable (to keep it lightly tensioned) and then round the hub and into the power connector on the board.

## Apply the kites

With the electronics finished, mount each kite by slipping the hollow kite over the LED, through the hole in the top, and then adding a stitch around the wires, through the kite to stop it sliding off again.
