/*
  AnalogReadSerial
  Reads an analog input on pin 0, prints the result to the serial monitor.
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

 This example code is in the public domain.
 */

#define SENSOR A0
#define DIAL 3
#define BUFFERLEN 32
#define RANGE 60
#define READDELAY 500UL
// BUFFERLEN * READDELAY = floor bracket time in ms
#define DEADZONE 3

int rolling_min[BUFFERLEN];
int buffer_pointer = 0;
int reading_floor = 0;
int sensorValue = 0;
unsigned long last_read = 0UL;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  analogReference(EXTERNAL);
  pinMode(DIAL, OUTPUT);
  
  //fill the rollinng_min buffer with max values
  for (int i=0; i<BUFFERLEN; i++) {
    rolling_min[i] = analogRead(SENSOR);
  }
  calcMin();
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  sensorValue = analogRead(SENSOR);
  // print out the value you read:
  Serial.println(sensorValue - reading_floor);
  analogWrite(DIAL, constrain(map(sensorValue, reading_floor+DEADZONE, reading_floor+DEADZONE+RANGE, 0,255), 0, 255));
  delay(1);        // delay in between reads for stability
  
  if (millis() - last_read >= READDELAY) {
    calcMin();
  }
}

void calcMin() 
{
  sensorValue = analogRead(SENSOR);
  rolling_min[buffer_pointer] = sensorValue;
  last_read = millis();
  buffer_pointer++;
  buffer_pointer = buffer_pointer % BUFFERLEN;
  // calculate the new minimum value
  int dividend = 0;
  int divisor = 0;
  for (int i=0; i<BUFFERLEN; i++) {
    if (rolling_min[i] < sensorValue) {
      dividend = dividend + rolling_min[i];
      divisor++;
    }
  }
  if (dividend > 0 && divisor > 0) {
    reading_floor = dividend/divisor;
  } else {
    reading_floor = sensorValue;
  }
}
