#include "FastLED.h"

// some case constants:
#define RISE 0
#define HOLD 1
#define FALL 2
#define PAUSE 3

// How many leds in your strip?
#define NUM_LEDS 42

// For led chips like Neopixels, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806 define both DATA_PIN and CLOCK_PIN
#define DATA_PIN 6

// Define the LED colour (only one for the whole strip
#define RED 0xFF
#define GREEN 0xF2
#define BLUE 0x73
// Set the max and min values for flicker
#define LOW_MIN 5
#define LOW_MAX 50
#define HIGH_MIN 5
#define HIGH_MAX 255
// hold length at high value
#define HOLD_MIN 20  //milliseconds
#define HOLD_MAX 150 //milliseconds
// pause length at low value
#define PAUSE_MAX 10 //milliseconds
#define PAUSE_MIN 50 //milliseconds
// rate of rise and fall
#define RISE_MIN 50 // dV_out/ms
#define RISE_MAX 1000   // dV_out/ms#include "FastLED.h"
#define FALL_MIN 50 // dV_out/ms
#define FALL_MAX 250 // dV_out/ms

const int high_max_range = HIGH_MAX - HIGH_MIN;
const int low_max_range = LOW_MAX - LOW_MIN;

// Define the array of leds
CRGB leds[NUM_LEDS];
int current_brightness[NUM_LEDS];
int target_brightness[NUM_LEDS];
uint8_t rate_of_change[NUM_LEDS];
uint8_t state[NUM_LEDS];
uint32_t change_time[NUM_LEDS];

#define SENSOR A0
#define DIAL 3
#define BUFFERLEN 32
#define RANGE 25
#define READDELAY 500UL
// BUFFERLEN * READDELAY = floor bracket time in ms
#define DEADZONE 3

int rolling_min[BUFFERLEN];
int buffer_pointer = 0;
int reading_floor = 0;
int sensorValue = 0;
unsigned long last_read = 0UL;

void setup() {
  //Serial.begin(9600);

  FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);

  for (int i=0; i<NUM_LEDS; i++) {
    current_brightness[i] = 0;
    target_brightness[i] = 0;
    rate_of_change[i] = 0;
    state[i] = PAUSE;
    change_time[i] = 0UL;
  }

  // set the analog ref to be external (so we can use a lower V_ref)
  //analogReference(EXTERNAL);

  // include the dial display for debugging
  pinMode(DIAL, OUTPUT);

  //fill the rollinng_min buffer with max values
  for (int i=0; i<BUFFERLEN; i++) {
    rolling_min[i] = analogRead(SENSOR);
  }
  calcMin();
}

void loop() {
  // read the input on analog pin 0:
  sensorValue = analogRead(SENSOR);
  // print out the value you read:
  //Serial.println(sensorValue - reading_floor);
  analogWrite(DIAL, constrain(map(sensorValue, reading_floor+DEADZONE, reading_floor+DEADZONE+RANGE, 0,255), 0, 255));
  delay(1);        // delay in between reads for stability

  if (millis() - last_read >= READDELAY) {
    calcMin();
  }

  //then run the animation (functionised for readability)
  animateLEDs();
}

void setBrightness(uint8_t led_number) {
  int num = (current_brightness[led_number] * RED);
  leds[led_number].r = num / 0xFF;
  leds[led_number].g = (current_brightness[led_number] * GREEN) / 0xFF;
  leds[led_number].b = (current_brightness[led_number] * BLUE) / 0xFF;
}

void calcMin()
{
  sensorValue = analogRead(SENSOR);
  rolling_min[buffer_pointer] = sensorValue;
  last_read = millis();
  buffer_pointer++;
  buffer_pointer = buffer_pointer % BUFFERLEN;
  // calculate the average value that's below the current reading
  int dividend = 0;
  int divisor = 0;
  for (int i=0; i<BUFFERLEN; i++) {
    if (rolling_min[i] < sensorValue) {
      dividend = dividend + rolling_min[i];
      divisor++;
    }
  }
  if (dividend > 0 && divisor > 0) {
    // if there are readings below the current one, use the average
    reading_floor = dividend/divisor;
  } else {
    // or this is the lowest value
    reading_floor = sensorValue;
  }
}

void animateLEDs()
{
  unsigned long frame_time = millis();
  int current_high_max = HIGH_MIN+(high_max_range * (sensorValue - reading_floor - DEADZONE) / RANGE );
  current_high_max = constrain(current_high_max, HIGH_MIN, HIGH_MAX);
  int current_low_max = LOW_MIN+(low_max_range * (sensorValue - reading_floor - DEADZONE) / RANGE );
  current_low_max = constrain(current_low_max, LOW_MIN, LOW_MIN);
  int current_high_min = HIGH_MIN+(high_max_range * (sensorValue - reading_floor - DEADZONE) / RANGE / 6);
  current_high_min = constrain(current_high_min, HIGH_MIN, HIGH_MAX);
  int current_low_min = LOW_MIN+(low_max_range * (sensorValue - reading_floor - DEADZONE) / RANGE / 6);
  current_low_min = constrain(current_low_min, LOW_MIN, LOW_MIN);
  //Serial.print("current_high_limit: ");
  //Serial.println(current_high_limit);
  //boolean echo = false;

  for (int i=0; i<NUM_LEDS; i++) {
    if (state[i] == RISE) {
      // increment the led level
      int no_steps = (frame_time - change_time[i])*100 / rate_of_change[i];
      if (no_steps > 0) {
        current_brightness[i] = current_brightness[i] + no_steps;
        change_time[i] = frame_time;
      }
      // if we've reached the max, change to HOLD mode
      if (current_brightness[i] >= target_brightness[i]) {
        current_brightness[i] = target_brightness[i];
        change_time[i] = frame_time + random(HOLD_MIN, HOLD_MAX);
        state[i] = HOLD;
      }
      setBrightness(i);
    } else if (state[i] == HOLD) {
      // wait until the next required change time
      if (frame_time >= change_time[i]) {
        //switch to FALL mode and calculate the rate of change
        rate_of_change[i] = random(FALL_MIN, FALL_MAX);
        if (rate_of_change[i] < FALL_MIN) rate_of_change[i] = FALL_MIN;
        target_brightness[i] = random(current_low_min, current_low_max);
        state[i] = FALL;
        //echo = true;
     }
    } else if (state[i] == FALL) {
      // decrement the led level
      int no_steps = (frame_time - change_time[i])*100 / rate_of_change[i];
      if (no_steps > 0) {
        current_brightness[i] = current_brightness[i] - no_steps;
        change_time[i] = frame_time;
      }
      // if we've reached the min, change to HOLD mode
      if (current_brightness[i] <= target_brightness[i]) {
        current_brightness[i] = target_brightness[i];
        change_time[i] = frame_time + random(PAUSE_MIN, PAUSE_MAX);
        state[i] = PAUSE;
      }
      setBrightness(i);
     } else if (state[i] == PAUSE) {
      // wait until the next required change time
      if (frame_time >= change_time[i]) {
        //switch to RISE mode and calculate the rate of change
        rate_of_change[i] = random(RISE_MIN, RISE_MAX);
        if (rate_of_change[i] < RISE_MIN) rate_of_change[i] = RISE_MIN;
        target_brightness[i] = random(current_high_min, current_high_max);
        state[i] = RISE;
        //echo = true;
      }
     } else {
      // reset the state to rise
      state[i] = PAUSE;
    }
  }

  //if (echo == true) {
    //for (int i=0; i<NUM_LEDS; i++) {
      //Serial.print(current_brightness[i]);
      //Serial.print(',');
    //}
    //Serial.println();
    //echo = false;
  //}

  // then display the frame
  FastLED.show();

  delay(1);
}
